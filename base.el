
(package-initialize)

(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/emacs-klere-theme")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/heroku-theme")

(require 'virtualenvwrapper)
(setq venv-location "~/work/jedienv")
(defun my/python-mode-hook ()
  (add-to-list 'company-backends 'company-jedi))

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mako\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

(add-hook 'python-mode-hook 'my/python-mode-hook)
(setq jedi:complete-on-dot t)
(add-hook 'after-init-hook 'global-company-mode)
(company-quickhelp-mode 1)
(require 'py-autopep8)
;(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(require 'magit)
(require 'evil)
(evil-mode 1)

(add-to-list 'load-path
	     "~/.emacs.d/elpa/yasnippet-snippets-0.4/"
	     "~/.emacs.d/elpa/yasnippet-0.13.0/")
(require 'yasnippet)
(yas-global-mode 1)

(global-flycheck-mode)

;(add-to-list
; 'default-frame-alist
; '(font . "Input Mono-11"))
(set-default-font "Roboto Mono-11" nil t)
(set-face-attribute 'default nil :height 100)

(require 'ace-jump-mode)
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

(require 'linum-relative)
(linum-on)

(setq read-file-name-completion-ignore-case t)
(require 'ido)
(ido-mode t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(display-time-mode)
(require 'smart-mode-line)
(display-time-mode)
(require 'smart-mode-line)
(setq powerline-arrow-shape 'curve)
(setq powerline-default-separator-dir '(right . left))
(setq sml/theme 'powerline)
(setq sml/mode-width 0)
(setq sml/name-width 20)
(rich-minority-mode 1)
(setf rm-blacklist "")
(sml/setup)
(setq column-number-mode t)
(require 'fill-column-indicator)
(fci-mode)

;; KEYboard
(global-set-key (kbd "C-<tab>") 'company-complete)
(global-set-key (kbd "C-x g") 'magit-status)
